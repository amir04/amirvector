#include "Vector.h"

Vector::Vector(int n)
{
	if (n < 2)
	{
		n = 2;
	}
	this->_elements = new int[n];
	this->_size = 0;
	this->_capacity = n;
	this->_resizeFactor = n;
}

Vector::~Vector()
{
	delete[] _elements;
	_elements = nullptr;
}

int Vector::size() const
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

bool Vector::empty() const
{
	return (this->_size == 0);
}


void Vector::push_back(const int& val)
{
	if (this->_size == this->_capacity)
	{
		reserve(_size + 1);
	}
	this->_elements[this->_size] = val;
	this->_size ++;
}

int Vector::pop_back()
{
	int value = 0;
	if (this->empty())
	{
		std::cerr << "error: pop from empty vector";
		return -9999;
	}
	this->_size--;
	value = this->_elements[this->_size];
	this->_elements[this->_size] = 0;
	return value;
}

void Vector::reserve(int n)
{
	int * TempArr;
	int i = 0, newsize = 0;
	//the new size of the arr is n (this is the base) + the length from the next duplicates of reseieFactor
	newsize = n + (_resizeFactor - n % _resizeFactor);
	TempArr = new int[newsize];
	for (i = 0; i < this->_capacity; i++)
	{
		TempArr[i] = this->_elements[i];
	}
	delete[] this->_elements;
	this->_capacity = newsize;
	this->_elements = TempArr;
}

void Vector::resize(int n)
{
	int i = 0;
	if (n <= _capacity)
	{
		for (i = n; i < _size; i++)
		{
			_elements[i] = 0;
		}
	}
	else
	{
		reserve(n);
	}
	this->_size = n;//its the new size, anyway
}

void Vector::assign(int val)
{
	int i = 0;
	//i put val in all the places until size
	for (i; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
}

void Vector::resize(int n, const int& val)
{
	resize(n);
	assign(val);
}

Vector::Vector(const Vector& other)
{
	int i = 0;
	*this = other;
}

Vector& Vector::operator=(const Vector& other)
{
	int i = 0;
	//check if elements is already defined
	if (_capacity)
	{
		delete[] this->_elements;
	}
	this->_elements = new int[other._capacity];
	this->_capacity = other._capacity;
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;
	//i copy the valus and not the pointer
	for (i = 0; i < this->_capacity; i++)
	{
		this->_elements[i] = other._elements[i];
	}
	return *this;
}


int& Vector::operator[](int n) const
{
	if (n >= _capacity)
	{
		std::cerr << n << "is a wrong place\n";
		return _elements[0];
	}
	return this->_elements[n];
}